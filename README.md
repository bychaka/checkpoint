o# Employees API

### Simple Express application

### First start
1. ```docker-compose up api-db```
2. When container from previous step is ready, run following command: ```sh ./restore-dump.sh```
3. Then run command `docker-compose up` or just `npm run docker:containers:start`

### Installation
For running application on your local machine we recomended use Docker.
For start application with Docker execute following command:
```
docker-compose up --build --force-recreate
```
This command create 2 containers:
```
- api (serve node application)
- api-db (serve MsSQL)
```

More details about Docker you can read here: https://docs.docker.com/

You also can serve application on your host. For running on your host be sure that 
you have installed NodeJS v.8+ and declare following env:
```
NODE_ENV: 'development'
PORT: 3000
DB_HOST: 'api-db'
DB_PORT: 1433
DB_NAME: 'employees'
DB_USER: 'root'
DB_USER_PASSWORD: 'root'
```

Then you cat start application executing next command:
```
npm run start
```
