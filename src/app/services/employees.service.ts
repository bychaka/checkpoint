import { inject, injectable } from 'inversify';
import { TYPES } from '@app/ioc/types';
import { catchableMethod } from '@app/decorators';
import { IEmployeesRepository } from '@app/repositories';

export interface IEmployeesService {
  getEmployeeSummaryByDate(employeeId: string, date: string): Promise<any>;
  getEmployees(): Promise<any>;
}

@injectable()
export class EmployeesService implements IEmployeesService {
  @inject(TYPES.EmployeesRepository) private employeesRepository: IEmployeesRepository;

  @catchableMethod()
  public async getEmployeeSummaryByDate(employeeId: string, date: string): Promise<any> {
    return await this.employeesRepository.getEmployeeSummaryByDate(employeeId, date);
  }

  @catchableMethod()
  public async getEmployees(): Promise<any> {
    return await this.employeesRepository.getEmployees();
  }
}
