import { Logger } from 'winston';
import { injectable, inject } from 'inversify';
import { ConnectionPool, IResult } from 'mssql';
import { TYPES } from '@app/ioc/types';
import { catchableMethod } from '@app/decorators';

@injectable()
export class BaseRepository {
  @inject(TYPES.LoggerService) protected loggerService: Logger;
  @inject(TYPES.MsSQLConnectionPool) protected connectionPool: ConnectionPool;

  @catchableMethod()
  protected async runQuery(query: TemplateStringsArray): Promise<IResult<any>> {
    return await this.connectionPool.query(query);
  }
}
