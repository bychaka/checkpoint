import * as moment from 'moment';
import { IRecordSet } from 'mssql';
import { injectable } from 'inversify';
import { NotFound } from '@app/errors';
import { catchableMethod } from '@app/decorators';
import { BaseRepository } from '@app/repositories';

export interface IEmployeesRepository {
  getEmployeeSummaryByDate(employeeId: string, date: string): Promise<any>;
  getEmployees(): Promise<any>;
}

@injectable()
export class EmployeesRepository extends BaseRepository implements IEmployeesRepository {
  @catchableMethod()
  public async getEmployeeSummaryByDate(employeeId: string, date: string): Promise<any> {
    const startDate = new Date(`${ date } 00:00:00`);
    const endDate = new Date(`${ date } 23:59:59`);
    const { recordset } = await this
      .connectionPool
      .query`SELECT *
             FROM SPGAcademy.dbo.Event
             WHERE EmployeeID = ${ parseInt(employeeId, 10) }
             AND EventDate between ${ startDate } and ${ endDate }`;

    if (recordset.length === 0) {
      throw new NotFound(`Events for employee: "${ employeeId }" with date: "${ date }" not found.`);
    }

    return {
      totalTime: moment(await this.getTotalTime(recordset, startDate, endDate)).format('HH:mm:ss'),
    };
  }

  @catchableMethod()
  public async getEmployees(): Promise<any> {
    const { recordset } = await this
      .connectionPool
      .query`SELECT  Ev.EmployeeID, Em.FIO, Ev.EventDescription , Ev.EventDescriptionID, FORMAT (Ev.EventDate, 'yyyy-MM-dd HH:mm') Date
      FROM ((SPGAcademy.dbo.Event AS Ev
      INNER JOIN (
       SELECT EmployeeID, MAX(EventID) as LastEvent
       FROM SPGAcademy.dbo.Event
       GROUP BY EmployeeID
      ) as En ON En.EmployeeID = Ev.EmployeeID AND En.LastEvent = Ev.EventID)
      INNER JOIN SPGAcademy.dbo.Employee as Em on Em.EmployeeID = Ev.EmployeeID)
      ORDER BY EmployeeID`;

    return recordset;
  }

  @catchableMethod()
  private async getTotalTime(recordset: IRecordSet<any>, startDate: Date, endDate: Date): Promise<number> {
    let begin = moment(0);
    const totalTime: moment.Duration = recordset.reduce((acc: moment.Duration, record, index: number) => {
      const eventDate = moment(record.EventDate);

      switch (record.EventDescriptionID) {
        case 45:
          if (recordset.length - 1 === index) {
            return acc.add(
              moment
                .duration(
                  moment(endDate)
                    .diff(eventDate),
                ),
            );
          }

          begin = moment(record.EventDate);

          return acc;
        case 46:
          return acc.add(
            moment
              .duration(
                index === 0 ?
                  eventDate.diff(startDate.getMilliseconds()) :
                  eventDate.diff(begin),
              ),
          );
      }
    }, moment.duration());

    return totalTime.asMilliseconds();
  }

  // ToDo: Implement another methods for repository
}
