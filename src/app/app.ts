import * as express from 'express';
import * as compression from 'compression';
import { TYPES } from '@app/ioc/types';
import { BaseRouter } from '@app/routers';
import { appContainer } from '@app/ioc/container';
import { connectToDB, enableCORS } from '@app/middlewares';

connectToDB();
const app = express();
app.disable('x-powered-by');
app.use(compression());
app.use(enableCORS()); // всем фронтам к аппке
const apiPathPrefix = '/api/v1';
app.use(`${apiPathPrefix}/employees`, appContainer.get<BaseRouter>(TYPES.EmployeesRouter).router);
app.use('/health', appContainer.get<BaseRouter>(TYPES.HealthRouter).router);
app.use('*', appContainer.get<BaseRouter>(TYPES.GuardRouter).router);

export { app };
