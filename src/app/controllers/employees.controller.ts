import { inject, injectable } from 'inversify';
import { NextFunction, Request, Response } from 'express';
import { TYPES } from '@app/ioc/types';
import { NotFound } from '@app/errors';
import { catchableAction } from '@app/decorators';
import { IEmployeesService } from '@app/services';

@injectable()
export class EmployeesController {
  @inject(TYPES.EmployeesService) private employeesService: IEmployeesService;

  @catchableAction()
  public async getEmployees(req: Request, res: Response, next: NextFunction): Promise<any> {
    return res.status(200).json(await this.employeesService.getEmployees());
  }

  @catchableAction()
  public async getEmployeeSummary(req: Request, res: Response, next: NextFunction): Promise<any> {
    return res.status(200).json(
      await this
        .employeesService
        .getEmployeeSummaryByDate(
          req.params.employeeId,
          req.params.date,
        ));
  }
}
