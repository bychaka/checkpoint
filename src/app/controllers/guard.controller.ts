import { injectable } from 'inversify';
import { NextFunction, Request, Response } from 'express';
import { NotFound } from '@app/errors';
import { catchableAction } from '@app/decorators';

@injectable()
export class GuardController {
  @catchableAction()
  public async processUnknownRoutes(req: Request, res: Response, next: NextFunction): Promise<void> {
    throw new NotFound(`Given path: "${ req.originalUrl }" is not correct.`);
  }
}
