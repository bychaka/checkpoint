import { ConnectionPool } from 'mssql';
import { inject, injectable } from 'inversify';
import { NextFunction, Request, Response } from 'express';
import { TYPES } from '@app/ioc/types';
import { catchableAction, catchableMethod } from '@app/decorators';

interface ICheckResult {
  check: string;
  successful: boolean;
  details?: any;
}

@injectable()
export class HealthController {
  @inject(TYPES.MsSQLConnectionPool) private connectionPool: ConnectionPool;

  @catchableAction()
  public async check(req: Request, res: Response, next: NextFunction): Promise<Response> {
    const results: ICheckResult[] = await Promise.all([
      this.getDBConnectionStatus(),
    ]);

    const failuredChecks: ICheckResult[] = [];
    for (const checkResult of results) {
      if (!checkResult.successful) {
        failuredChecks.push(checkResult);
      }
    }

    if (failuredChecks.length > 0) {
      return res.status(200).json({ health: 'not ok', failuredChecks });
    }

    return res.status(200).json({ health: 'ok', results });
  }

  @catchableMethod()
  private async getDBConnectionStatus(): Promise<ICheckResult> {
    return {
      check: 'MsSQL Connection',
      successful: this.connectionPool.connected,
    };
  }
}
