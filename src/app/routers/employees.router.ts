import { inject, injectable } from 'inversify';
import { TYPES } from '@app/ioc/types';
import { BaseRouter } from '@app/routers';
import { EmployeesController } from '@app/controllers';

@injectable()
export class EmployeesRouter extends BaseRouter {
  public constructor(@inject(TYPES.EmployeesController) private employeesController: EmployeesController) {
    super();

    this.defineRoutes();
  }

  protected defineRoutes(): void {
    this.router.get(
      '',
      this.employeesController.getEmployees.bind(this.employeesController),
    );

    this.router.get(
      '/:employeeId/summary/:date',
      this.employeesController.getEmployeeSummary.bind(this.employeesController),
    );
  }
}
