import { inject, injectable } from 'inversify';
import { TYPES } from '@app/ioc/types';
import { BaseRouter } from '@app/routers';
import { GuardController } from '@app/controllers';

@injectable()
export class GuardRouter extends BaseRouter {
  public constructor(@inject(TYPES.GuardController) private guardController: GuardController) {
    super();

    this.defineRoutes();
  }

  protected defineRoutes(): void {
    this.router.use('*', this.guardController.processUnknownRoutes.bind(this.guardController));
  }
}
