import { Router } from 'express';
import { injectable } from 'inversify';

@injectable()
export abstract class BaseRouter {
  // tslint:disable
  // @ts-ignore
  protected _router: Router;
  // tslint:enable

  protected constructor() {
    this.router = Router();
  }

  public get router(): Router {
    return this._router;
  }

  public set router(router: Router) {
    this._router = router;
  }

  protected abstract defineRoutes(): void;
}
