import { inject, injectable } from 'inversify';
import { TYPES } from '@app/ioc/types';
import { BaseRouter } from '@app/routers';
import { HealthController } from '@app/controllers';

@injectable()
export class HealthRouter extends BaseRouter {
  public constructor(@inject(TYPES.HealthController) private healthController: HealthController) {
    super();

    this.defineRoutes();
  }

  protected defineRoutes(): void {
    this.router.get('/check', this.healthController.check.bind(this.healthController));
  }
}
