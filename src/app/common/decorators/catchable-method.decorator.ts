import { Logger } from 'winston';
import { TYPES } from '@app/ioc/types';
import { appContainer } from '@app/ioc/container';

/**
 * Decorator wraps class method in to try/catch block for error handling.
 * Catched exception will be logged and then throw to the next abstract level.
 */
export const catchableMethod = (): (...args: any[]) => {} => {
  return function(target: any, key: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const method = descriptor.value;

    descriptor.value = async function(...args: any[]): Promise<any> {
      try {
        return await method.apply(this, args);
      } catch (err) {
        appContainer
          .get<Logger>(TYPES.LoggerService)
          .error(
            `Error occurred during execute ${ target.constructor.name }::${ key }().` +
            `Message: ${ err.message }`,
          );

        throw err;
      }
    };

    return descriptor;
  };
};
