import { Logger } from 'winston';
import { Response } from 'express';
import {
  BadRequest,
  InternalError,
  NotFound,
  Unauthorized,
  ValidationError,
} from '@app/errors';
import { TYPES } from '@app/ioc/types';
import { appContainer } from '@app/ioc/container';

/**
 * Decorator wraps controller's action (Express middleware) in to try/catch block for error handling.
 * Catched exception will be logged and then send response with suitable http status code.
 */
export const catchableAction = (errorConstructor?: any, msg?: string): (...args: any[]) => {} => {
  return function(target: any, key: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const method = descriptor.value;

    descriptor.value = async function(...args: any[]): Promise<Response> {
      const res: Response = args[1];
      try {
        return await method.apply(this, args);
      } catch (err) {
        appContainer.get<Logger>(TYPES.LoggerService).error(
          `Error occurred during execute ${ target.constructor.name }::${ key }(). ` +
          `Message: ${ err.message }`,
        );

        if (errorConstructor) {
          err = new errorConstructor(msg);
        }

        switch (err.constructor) {
          case BadRequest:
            return res.status(400).json({ message: err.message });
          case Unauthorized:
            return res.status(401).json({ message: err.message });
          case NotFound:
            return res.status(404).json({ message: err.message });
          case ValidationError:
            return res.status(422).json({ message: err.message });
          case InternalError:
            return res.status(500).json({ message: err.message });
          default:
            return res.status(500).json({ message: 'Internal server error.' });
        }
      }
    };

    return descriptor;
  };
};
