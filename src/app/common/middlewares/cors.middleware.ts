import { NextFunction, Request, Response, RequestHandler } from 'express';

export const enableCORS = (): RequestHandler => {
  return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization',
    );
    res.setHeader(
      'Access-Control-Allow-Methods',
      'GET, POST, DELETE, PUT, OPTIONS',
    );

    return next();
  };
};
