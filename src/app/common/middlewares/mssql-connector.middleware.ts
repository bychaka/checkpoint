// tslint:disable:max-line-length
import { Logger } from 'winston';
import { ConnectionPool } from 'mssql';
import { TYPES } from '@app/ioc/types';
import { appContainer } from '@app/ioc/container';

export const connectToDB = () => {
  const pool = appContainer.get<ConnectionPool>(TYPES.MsSQLConnectionPool);

  pool.connect()
    .then((conn: ConnectionPool) => {
      appContainer
        .get<Logger>(TYPES.LoggerService)
        // @ts-ignore
        .debug(`Sucessfully connected to mssql://${ conn.config.user }:${ conn.config.password }@${ conn.config.server }:${ conn.config.port }/${ conn.config.database }.`);
    })
    .catch((err: any) => {
      // tslint:disable
      console.log('err', err);
      appContainer
        .get<Logger>(TYPES.LoggerService)
        .error(err.message);

      process.exit(1);
    });
};
