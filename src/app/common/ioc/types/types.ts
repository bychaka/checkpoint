const TYPES = {
  // Routers
  EmployeesRouter: Symbol.for('EmployeesRouter'),
  GuardRouter: Symbol.for('GuardRouter'),
  HealthRouter: Symbol.for('HealthRouter'),

  // Controllers
  EmployeesController: Symbol.for('EmployeesController'),
  GuardController: Symbol.for('GuardController'),
  HealthController: Symbol.for('HealthController'),

  // Repositories
  EmployeesRepository: Symbol.for('EmployeesRepository'),

  // Services
  LoggerService: Symbol.for('LoggerService'),
  EmployeesService: Symbol.for('EmployeesService'),

  // Connection pools
  MsSQLConnectionPool: Symbol.for('MsSQLConnectionPool'),
};

export { TYPES };
