import { Container } from 'inversify';
import {
  routers,
  services,
  controllers,
  repositories,
  connectionPools,
} from './modules';

const appContainer = new Container();
appContainer.load(
  // Register services
  services,
  // Register Routers
  routers,
  // Register Controllers,
  controllers,
  // Register connection pools
  connectionPools,
  // Register repositories
  repositories,
);

export { appContainer };
