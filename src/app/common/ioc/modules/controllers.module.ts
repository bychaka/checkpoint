import { ContainerModule, interfaces } from 'inversify';
import { TYPES } from '@app/ioc/types';
import {
  GuardController,
  HealthController,
  EmployeesController,
} from '@app/controllers';

export const controllers = new ContainerModule((bind: interfaces.Bind) => {
  bind<EmployeesController>(TYPES.EmployeesController).to(EmployeesController).inSingletonScope();
  bind<GuardController>(TYPES.GuardController).to(GuardController).inSingletonScope();
  bind<HealthController>(TYPES.HealthController).to(HealthController).inSingletonScope();
});
