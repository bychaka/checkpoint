import { ContainerModule, interfaces } from 'inversify';
import { TYPES } from '@app/ioc/types';
import {
  BaseRouter,
  GuardRouter,
  HealthRouter,
  EmployeesRouter,
} from '@app/routers';

export const routers = new ContainerModule((bind: interfaces.Bind) => {
  bind<BaseRouter>(TYPES.EmployeesRouter).to(EmployeesRouter).inSingletonScope();
  bind<BaseRouter>(TYPES.GuardRouter).to(GuardRouter).inSingletonScope();
  bind<BaseRouter>(TYPES.HealthRouter).to(HealthRouter).inSingletonScope();
});
