import { ContainerModule, interfaces } from 'inversify';
import { TYPES } from '@app/ioc/types';
import { EmployeesRepository } from '@app/repositories';

export const repositories = new ContainerModule((bind: interfaces.Bind) => {
  bind<EmployeesRepository>(TYPES.EmployeesRepository).to(EmployeesRepository).inSingletonScope();
});
