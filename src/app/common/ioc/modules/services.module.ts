import { ContainerModule, interfaces } from 'inversify';
import * as winston from 'winston';
import { TYPES } from '@app/ioc/types';
import { loggerOptions } from '@app/config';
import { EmployeesService } from '@app/services';

export const services = new ContainerModule((bind: interfaces.Bind) => {
  bind<EmployeesService>(TYPES.EmployeesService).to(EmployeesService).inSingletonScope();
  bind<winston.Logger>(TYPES.LoggerService).toConstantValue(winston.createLogger(loggerOptions));
});
