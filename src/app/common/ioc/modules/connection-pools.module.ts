import { ConnectionPool } from 'mssql';
import { ContainerModule, interfaces } from 'inversify';
import { TYPES } from '@app/ioc/types';
import { dbConnectionParams } from '@app/config';

export const connectionPools = new ContainerModule((bind: interfaces.Bind) => {
  bind<ConnectionPool>(TYPES.MsSQLConnectionPool).toConstantValue(new ConnectionPool(dbConnectionParams));
});
