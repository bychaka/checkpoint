import { LoggerOptions, format, transports } from 'winston';

const loggerOptions: LoggerOptions = {
  level: process.env.LOG_LEVEL || 'debug',
  format: format.json(),
  transports: [
    new transports.Console({
      format: format.simple(),
    }),
  ],
};

export { loggerOptions };
