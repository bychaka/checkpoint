import { config } from 'mssql';

export const dbConnectionParams: config = {
  server: process.env.DB_HOST as string,
  port: parseInt(process.env.DB_PORT as string, 10),
  user: process.env.DB_USER as string,
  password: process.env.DB_USER_PASSWORD as string,
  database: process.env.DB_NAME as string,
  parseJSON: true,
};
