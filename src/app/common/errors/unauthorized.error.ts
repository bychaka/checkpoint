export class Unauthorized extends Error {
  public constructor(msg: string = 'Unauthorized.') {
    super(msg);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, Unauthorized.prototype);
  }
}
