export class NotFound extends Error {
  public constructor(msg: string = 'Not found.') {
    super(msg);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, NotFound.prototype);
  }
}
