export class InternalError extends Error {
  public constructor(msg: string = 'Internal server error.') {
    super(msg);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, InternalError.prototype);
  }
}
