export class BadRequest extends Error {
  public constructor(msg: string = 'Bad request.') {
    super(msg);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, BadRequest.prototype);
  }
}
