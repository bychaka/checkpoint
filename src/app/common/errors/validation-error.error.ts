export class ValidationError extends Error {
  public constructor(msg: string = 'Validation error.') {
    super(msg);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, ValidationError.prototype);
  }
}
