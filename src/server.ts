// tslint:disable-next-line
require('module-alias/register');
import * as Promise from 'bluebird';
global.Promise = Promise;
import 'reflect-metadata';
import * as http from 'http';
import { Logger } from 'winston';
import { InversifyExpressServer } from 'inversify-express-utils';
import { app } from '@app';
import { TYPES } from '@app/ioc/types';
import { appContainer } from '@app/ioc/container';

const normalizePort = (val: any) => {
  const serverPort = parseInt(val, 10);

  if (!isNaN(serverPort)) {
    return val;
  }

  if (serverPort >= 0) {
    return serverPort;
  }

  return false;
};

const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + port;
  appContainer.get<Logger>(TYPES.LoggerService).debug('Listening on: ' + bind);
};

const onError = (error: any) => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const addr = server.address();

  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + port;

  switch (error.code) {
    case 'EACCES':
      appContainer.get<Logger>(TYPES.LoggerService).error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      appContainer.get<Logger>(TYPES.LoggerService).error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      appContainer.get<Logger>(TYPES.LoggerService).error(error);
      throw error;
  }
};

const port = normalizePort(process.env.PORT) || 3000;

app.set('port', port);

const inversifyServer = new InversifyExpressServer(appContainer, null, null, app, null, false);

const server = http.createServer(inversifyServer.build());
server.on('listening', onListening);
server.on('error', onError);
server.listen(port);

process.on('unhandledRejection', (reason: any, promise: any) => {
  appContainer
    .get<Logger>(TYPES.LoggerService)
    .crit(`Unhandled Rejection at:`, promise, reason);
});

process.on('uncaughtException', (error: Error) => {
  appContainer
    .get<Logger>(TYPES.LoggerService)
    .crit(`Uncaught Exception at:`, error.message);
});
