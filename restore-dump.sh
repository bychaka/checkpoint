#!/usr/bin/env bash

docker exec -it api-db /opt/mssql-tools/bin/sqlcmd \
     -S localhost \
     -U SA \
     -P 'StrongPassw0rd' \
     -Q 'RESTORE DATABASE SPGAcademy FROM DISK = "/var/opt/mssql/backup/SPGAcademy.bak" WITH MOVE "testdb" TO "/var/opt/mssql/data/academy.mdf", MOVE "testdb_log" TO "/var/opt/mssql/data/academy_log.mdf"'

#sudo docker exec -it api-db /opt/mssql-tools/bin/sqlcmd    -S localhost -U SA -P 'StrongPassw0rd'    -Q 'SELECT * FROM SPGAcademy.dbo.Event where EventDate > "2015-01-27 15:25:00"'